#pragma once

#include <lux_shared/common.hpp>

namespace deferred_renderer {

void init();
void deinit();

void bind_draw_buffers();
void unbind_draw_buffers();
void bind_buffer_textures(U32 texture_id);
void blit_depth_buffer();
void clear_screen();

}
