#include <glm/gtc/type_ptr.hpp>
//
#include <lux_shared/common.hpp>
//
#include <gl.hpp>
#include <deferred_renderer.hpp>
#include <directional_lightning.hpp>

namespace directional_lightning {

namespace {
#pragma pack(push, 1)
    struct Vert {
        Vec2F pos;
    };
#pragma pack(pop)

    gl::IdxBuff     i_buff;
    gl::VertBuff    v_buff;
    gl::VertFmt     vert_fmt;
    gl::VertContext context;

    GLuint program;
};

void init() {
    gl::VertContext::unbind_all();

    v_buff.init();
    v_buff.bind();
    v_buff.write(4, quad<F32>, GL_STATIC_DRAW);

    i_buff.init();
    i_buff.bind();
    i_buff.write(6, quad_idxs<U32>, GL_STATIC_DRAW);

    program = load_program("glsl/directional_lightning.vert",
                           "glsl/directional_lightning.frag");
    vert_fmt.init({{2, GL_FLOAT, false, false}}),
    context.init({v_buff}, vert_fmt);
}

void deinit() {
    context.deinit();
    i_buff.deinit();
    v_buff.deinit();
}

void render(Vec3F camera_pos, Source const& source) {
    glUseProgram(program);
    deferred_renderer::bind_buffer_textures(0);
    set_uniform("g_pos", program, glUniform1i, 0);
    set_uniform("g_norm", program, glUniform1i, 1);
    set_uniform("g_col" , program, glUniform1i, 2);
    set_uniform("camera_pos", program, glUniform3fv,
                1, glm::value_ptr(camera_pos));
    set_uniform("light.col", program, glUniform3fv,
                1, glm::value_ptr(source.col));
    set_uniform("light.dir", program, glUniform3fv,
                1, glm::value_ptr(source.dir));

    context.bind();
    i_buff.bind();
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

}
