#pragma once

#include <lux_shared/common.hpp>
#include <lux_shared/entity.hpp>
#include <lux_shared/net/data.hpp>
//
#include <ui.hpp>

struct EntityComps {
    typedef EntityVec Pos;
    typedef DynArr<char> Name;
    struct Model {
        U32   id;
    };
    using Health    = NetSsTick::Entities::Health;
    using Inventory = NetSsTick::Entities::Inventory;

    IdMap<EntityId, Pos>       pos;
    IdMap<EntityId, Name>      name;
    IdMap<EntityId, Model>     model;
    IdMap<EntityId, Health>    health;
    IdMap<EntityId, Inventory> inventory;
    IdMap<EntityId, BlockId>   voxel_item;
    IdMap<EntityId, F32>       stack;
};

extern EntityComps& entity_comps;
extern DynArr<EntityId> entities;
void entity_init();
void set_net_entities(NetSsTick::Entities&& net_comps);
namespace entity {
Vec3F get_player_pos();
void render(glm::mat4 const& transform, Slice<EntityId>);
}
