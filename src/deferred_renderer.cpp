#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/quaternion.hpp>
#include <imgui/imgui.h>
//
#include <lux_shared/common.hpp>
//
#include <gl.hpp>
#include <deferred_renderer.hpp>
#include <ui.hpp>
#include <client.hpp>

namespace deferred_renderer {

namespace {
    GLuint g_buff;
    GLuint g_pos;
    GLuint g_norm;
    GLuint g_col;
    GLuint rbo;
}

void init() {
    glGenFramebuffers(1, &g_buff);
    glBindFramebuffer(GL_FRAMEBUFFER, g_buff);

    glGenTextures(1, &g_pos);
    glBindTexture(GL_TEXTURE_2D, g_pos);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, 1600, 900, //@TODO screen sz
                 0, GL_RGBA, GL_FLOAT, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
                           g_pos, 0);

    glGenTextures(1, &g_norm);
    glBindTexture(GL_TEXTURE_2D, g_norm);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, 1600, 900, //@TODO screen sz
                 0, GL_RGB, GL_BYTE, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D,
                           g_norm, 0);

    glGenTextures(1, &g_col);
    glBindTexture(GL_TEXTURE_2D, g_col);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, 1600, 900, //@TODO screen sz
                 0, GL_RGB, GL_FLOAT, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D,
                           g_col, 0);

    glGenRenderbuffers(1, &rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, 1600, 900); //@TODO screen sz
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                              GL_RENDERBUFFER, rbo);
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        LUX_FATAL("failed to create framebuffer for deferred renderer");
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
}

void deinit() {
    LUX_UNIMPLEMENTED();
}

void bind_draw_buffers() {
    glBindFramebuffer(GL_FRAMEBUFFER, g_buff);

    constexpr GLenum buffers[3] =
        {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2};
    glDrawBuffers(3, buffers);

    glClearColor(1.f, 1.f, 1.f, 1.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void unbind_draw_buffers() {
    constexpr GLenum default_buffer[1] = {GL_COLOR_ATTACHMENT0};
    glDrawBuffers(1, default_buffer);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void bind_buffer_textures(U32 texture_id) {
    glActiveTexture(GL_TEXTURE0 + texture_id);
    glBindTexture(GL_TEXTURE_2D, g_pos);
    glActiveTexture(GL_TEXTURE1 + texture_id);
    glBindTexture(GL_TEXTURE_2D, g_norm);
    glActiveTexture(GL_TEXTURE2 + texture_id);
    glBindTexture(GL_TEXTURE_2D, g_col);
}

void blit_depth_buffer() {
    glBindFramebuffer(GL_READ_FRAMEBUFFER, g_buff);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glBlitFramebuffer(0, 0, 1600, 900, 0, 0, 1600, 900, GL_DEPTH_BUFFER_BIT,
                      GL_NEAREST); //@TODO screen sz
    glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
}

void clear_screen() {
    glClearColor(0.f, 0.f, 0.f, 0.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

}
