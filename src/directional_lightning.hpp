#pragma once

#include <lux_shared/common.hpp>

namespace directional_lightning {

struct Source {
    Vec3F col;
    Vec3F dir;
};

void init();
void deinit();
void render(Vec3F camera_pos, Source const& source);

}
