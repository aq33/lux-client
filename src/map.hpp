#pragma once

#include <lux_shared/map.hpp>
#include <lux_shared/net/data.hpp>
//
#include <db.hpp>
#include <ui.hpp>

extern VecSet<ChkPos> chunk_mesh_requests;

//this is for ui elements that want to render the map
void map_io_tick(ui::Handle, ui::Transform const&, ui::IoContext&);
void map_init();
void map_load_meshes(NetSsSgnl::ChunkMeshLoad const& net_meshes);
void map_update_meshes(NetSsSgnl::ChunkMeshUpdate const& net_meshes);

namespace map {

void render(Vec3F const& camera_pos, glm::mat4 const& transform, U32 render_dist);
void set_entities_chunk_map(VecMap<ChkPos, DynArr<EntityId>>&&);

}
