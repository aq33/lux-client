out vec4 o_col;

struct GBuffs {
    sampler2D pos;
    sampler2D norm;
    sampler2D col;
};

struct Light {
    vec3 pos;
    vec3 col;
    vec3 rad;
};

uniform GBuffs g_buffs;
uniform vec3   camera_pos;
uniform Light  light;

void main()
{
    const vec2 screen_size = vec2(1600.0, 900.0); //@TODO
    vec2 tex_pos = gl_FragCoord.xy / screen_size;
    vec3 pos  = texture(g_buffs.pos , tex_pos).xyz;
    vec3 norm = texture(g_buffs.norm, tex_pos).xyz;
    vec3 col  = texture(g_buffs.col , tex_pos).rgb;

    vec3 dir = normalize(light.pos - pos);
    float diff = max(dot(norm, dir), 0.0);
    //@TODO reuse length
    float distance = length(light.pos - pos);

    const float falloff = 10.0;
    const float f = 1.0 / falloff;
    float t = (sqrt(1.0 + 4.0 / f) - 1.0) / (2.0 * light.rad);
    float d = distance;
    float attenuation = ((1.0 + f) / (1 + (t * d) * (1 + (t * d)))) - f;

    vec3 view_dir = normalize(camera_pos - pos);
    vec3 halfway_dir = normalize(dir + view_dir);  
    float spec = pow(max(dot(norm, halfway_dir), 0.0), 64.0);

    o_col = vec4(light.col * attenuation * col * (0.5 * spec + diff), 1.0);
}
