layout (location = 0) out vec4 g_pos;
layout (location = 1) out vec3 g_norm;
layout (location = 2) out vec3 g_col;

in vec3 f_map_pos;
in vec2 f_tex_pos;
in vec2 f_uv;
in vec3 f_norm;

uniform sampler2D tileset;
uniform vec2 tex_scale;

void main() {
    g_pos = vec4(f_map_pos, gl_FragCoord.z / gl_FragCoord.w);
    g_norm = f_norm;
    g_col = texture(tileset, f_tex_pos + fract(f_uv) * tex_scale).rgb;
}
