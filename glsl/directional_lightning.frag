out vec4 o_col;

in vec2 f_tex_pos;

struct Light {
    vec3 col;
    vec3 dir;
};

uniform sampler2D g_pos;
uniform sampler2D g_norm;
uniform sampler2D g_col;
uniform Light  light;
uniform vec3   camera_pos;

void main()
{
    vec4 pos = texture(g_pos, f_tex_pos);
    vec3 col = texture(g_col, f_tex_pos).rgb;
    if(pos.w != 1.0) {
        vec3 norm = texture(g_norm, f_tex_pos).xyz;
        vec3 dir = normalize(-light.dir);

        float diff = max(dot(norm, dir), 0.0);

        vec3 view_dir = normalize(camera_pos - pos.xyz);
        vec3 halfway_dir = normalize(dir + view_dir);  

        float spec = pow(max(dot(norm, halfway_dir), 0.0), 1.0);
        vec3 l_col = light.col * col * (0.1 + diff + 0.5 * spec);
        float fog = clamp(exp(-0.000003 * pos.w * pos.w), 0., 1.);
        o_col = vec4(mix(light.col, l_col, fog), 1.0);
    } else {
        //maybe we should just set spec to 0 and use regular way
        o_col = vec4(light.col * col, 1.0);
    }
}
